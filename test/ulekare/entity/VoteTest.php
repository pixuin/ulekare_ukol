<?php

namespace ulekare\entity;


use DateTime;
use PHPUnit\Framework\TestCase;

class VoteTest extends TestCase
{
    public function testGetters(): void
    {
        $date = new DateTime();
        $vote = new Vote('question', 'response', 'firstname', 'lastname', $date);

        $this->assertEquals('question', $vote->getQuestion());
        $this->assertEquals('response', $vote->getResponse());
        $this->assertEquals('firstname', $vote->getFirstName());
        $this->assertEquals('lastname', $vote->getLastName());
        $this->assertEquals($date, $vote->getDate());
    }

}
