<?php

namespace ulekare\reader;


use PHPUnit\Framework\TestCase;

class CsvReaderFactoryTest extends TestCase
{

    public function testCreate()
    {
        $this->assertInstanceOf(CsvReader::class, CsvReaderFactory::create(__DIR__ . '/data/votes.csv'));
    }
}
