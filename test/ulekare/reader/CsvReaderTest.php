<?php declare(strict_types=1);

namespace reader;

use InvalidArgumentException;
use Iterator;
use PHPUnit\Framework\TestCase;
use ulekare\reader\CsvReader;

class CsvReaderTest extends TestCase
{
    public function testImplementIterator(): void
    {
        $csvReader = new CsvReader(__DIR__ . '/data/votes.csv');
        $this->assertInstanceOf(Iterator::class, $csvReader);
    }

    public function test__construct(): void
    {
        $csvFilePath = __DIR__ . '/data/not-exist-file.csv';
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf('%s is not file', $csvFilePath));
        $this->expectExceptionCode(CsvReader::ERROR_IS_NOT_FILE);
        new CsvReader($csvFilePath, ';');
    }

    public function testValid(): void
    {
        $csvFilePath = __DIR__ . '/data/votes.csv';
        $csvReader = new CsvReader($csvFilePath, ';');

        $this->assertTrue($csvReader->valid());

        foreach ($csvReader as $row) {
        }
        $this->assertFalse($csvReader->valid());

    }

    public function testRewind(): void
    {
        $csvFilePath = __DIR__ . '/data/votes.csv';
        $csvReader = new CsvReader($csvFilePath, ';');

        $csvReader->next();
        $csvReader->rewind();
        $this->assertEquals(0, $csvReader->key());
    }

    public function testKey(): void
    {
        $this->testNext();
    }

    public function testNext(): void
    {
        $csvFilePath = __DIR__ . '/data/votes.csv';
        $csvReader = new CsvReader($csvFilePath, ';');

        $csvReader->next();
        $this->assertEquals(1, $csvReader->key());

        $csvReader->next();
        $this->assertEquals(2, $csvReader->key());
    }

    public function testCurrent(): void
    {
        $csvFilePath = __DIR__ . '/data/votes.csv';
        $csvReader = new CsvReader($csvFilePath, ';');

        $csvReader->valid();

        $this->assertEquals([
            'question',
            'response',
            'first_name',
            'last_name',
            'date',
        ], $csvReader->current());
    }
}
