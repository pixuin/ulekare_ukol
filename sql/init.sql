CREATE TABLE `poll_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `response_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `question` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `response` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `response` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


ALTER TABLE `poll_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `response_id` (`response_id`);

ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `question_2` (`question`),
  ADD KEY `question` (`question`);

ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `response_2` (`response`),
  ADD KEY `response` (`response`);


ALTER TABLE `poll_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `question`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `response`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;


ALTER TABLE `poll_log`
  ADD CONSTRAINT `poll_log_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  ADD CONSTRAINT `poll_log_ibfk_2` FOREIGN KEY (`response_id`) REFERENCES `response` (`id`);
