# README #

## Installation:
1. Copy env sample:
    ```shell script
    cp env-sample .env
    ``` 
2. Set correct MySQL connection in .env file.

    ```shell script
    composer install 
    ```

3. Check tests:
    ```shell script
    composer test
    ```

4. Prepare DB structure:
    ```shell script
    mysql -u[user] -p < ./sql/init.sql
    ```
   
5. Set correct permissions for script: 
    ```shell script
    chmod +x ./import-poll
    ```

## Usage
1. Help: 
    ```shell script
    # for help how to use type
    ./import-poll 
    ```
2. Usage:
    ```shell script
     ./bin/import-poll [filepath imported CSV file]
    ```
