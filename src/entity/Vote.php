<?php declare(strict_types=1);

namespace ulekare\entity;

use DateTime;

class Vote
{
    private $question;
    private $response;
    private $first_name;
    private $last_name;
    private $date;

    public function __construct(string $question, string $response, string $first_name, string $last_name, DateTime $date)
    {
        $this->question = $question;
        $this->response = $response;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->date = $date;
    }

    public function toArray()
    {
        return [
            'question' => $this->getQuestion(),
            'response' => $this->getResponse(),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'date' => $this->getDate(),
        ];
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function getFirstName(): string
    {
        return $this->first_name;
    }

    public function getLastName(): string
    {
        return $this->last_name;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }
}
