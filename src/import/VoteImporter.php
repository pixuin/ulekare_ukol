<?php declare(strict_types=1);

namespace ulekare\import;

use DateTime;
use Iterator;
use RuntimeException;
use ulekare\entity\Vote;
use ulekare\helper\Time;
use ulekare\repository\VoteRepository;

class VoteImporter implements Importer
{

    protected const CSV_DATE_FORMAT = 'd.m.Y';

    private $countImported = 0;
    private $countByDate = [];
    private $questions = [];
    private $respondents = [];
    private $voteRepository;
    private $buffer = [];

    public function __construct(VoteRepository $voteRepository)
    {

        $this->voteRepository = $voteRepository;
    }

    public function import(Iterator $iterator, bool $hasHeader = true, int $saveInterval = 10): void
    {
        $this->buffer = [];
        $header = [];
        foreach ($iterator as $key => $item) {
            if ($hasHeader === true) {
                if ($key === 0) {
                    $header = $item;
                    continue;
                }

                $vote = $this->mapByHeader($header, $item);
                $this->addToBuffer($vote);
                $this->applyStatistics($vote);


                if (count($this->buffer) % $saveInterval === 0) {
                    $this->processBuffer();
                }
            } else {
                throw new RuntimeException('Not implemented yet'); // nema vyznam pro ukazku dal resit jiny typ importu ;)
            }
        }
        if (empty($this->buffer) === false) {
            $this->processBuffer();
        }
    }

    private function mapByHeader(array $header, array $item): array
    {
        $vote = [];
        foreach ($header as $k => $column) {
            $vote[$column] = $item[$k];
        }
        return $vote;
    }

    private function addToBuffer(array $vote): void
    {
        $voteDate = DateTime::createFromFormat(self::CSV_DATE_FORMAT, $vote['date']);
        $this->buffer[] = new Vote(
            $vote['question'],
            $vote['response'],
            $vote['first_name'],
            $vote['last_name'],
            $voteDate
        );
    }

    private function applyStatistics(array $vote): void
    {
        if (isset($this->countByDate[$vote['date']])) {
            $this->countByDate[$vote['date']]++;
        } else {
            $this->countByDate[$vote['date']] = 1;
        }
        if (isset($this->questions[$vote['question']][$vote['date']])) {
            $this->questions[$vote['question']][$vote['date']]++;
        } else {
            $this->questions[$vote['question']][$vote['date']] = 1;
        }

        $this->respondents[$vote['first_name'] . ' ' . $vote['last_name']] = Time::elapsed(DateTime::createFromFormat(self::CSV_DATE_FORMAT, $vote['date']));
    }

    private function processBuffer(): void
    {
        $this->voteRepository->addMultiple($this->buffer);
        $this->countImported += count($this->buffer);
        $this->buffer = [];
    }

    public function getImportStatistics(): string
    {

        $importedQuestions =
            PHP_EOL . 'Seznam všech otázek a počet odpovědí na otázku podle data:' . PHP_EOL;
        foreach ($this->questions as $question => $questionByDate) {
            $importedQuestions .= '   ' . $question . PHP_EOL;
            foreach ($questionByDate as $date => $count) {
                $importedQuestions .= sprintf('      %s: %s', $date, $count) . PHP_EOL;
            }
        }

        $respondents = PHP_EOL . 'Respondenti:' . PHP_EOL;
        foreach ($this->respondents as $respondent => $voteTIme) {
            $respondents .= sprintf("\t%s:\t\t%s%s", $respondent, $voteTIme, PHP_EOL);
        }

        return 'STATISTIKA:' . PHP_EOL
            . '-----------' . PHP_EOL
            . sprintf('Celkový počet odpovědí na každou z otázek: %s' . PHP_EOL
                . ''
                , $this->countImported)
            . $importedQuestions . $respondents;
    }
}
