<?php declare(strict_types=1);

namespace ulekare\import;

use Iterator;

interface Importer
{
    public function import(Iterator $iterator, bool $hasHeader = true): void;

    public function getImportStatistics(): string;
}
