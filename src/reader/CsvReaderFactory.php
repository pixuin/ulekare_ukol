<?php declare(strict_types=1);

namespace ulekare\reader;

class CsvReaderFactory
{
    public static function create(string $csvFilePath, string $delimiter = ';', int $rowLength = 1000): CsvReader
    {
        return new CsvReader($csvFilePath, $delimiter, $rowLength);
    }
}
