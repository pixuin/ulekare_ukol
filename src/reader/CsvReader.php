<?php declare(strict_types=1);

namespace ulekare\reader;


use InvalidArgumentException;
use Iterator;

class CsvReader implements Iterator
{
    public const ERROR_IS_NOT_FILE = 55001;
    public const ERROR_IS_NOT_READABLE = 55002;
    public const ERROR_CANNOT_OPEN_FILE = 55003;

    private $position = 0;

    private $fileResource;
    private $delimiter;
    private $csvRowData;
    private $rowLength;

    public function __construct(string $csvFilePath, string $delimiter = ';', int $rowLength = 1000)
    {
        if (is_file($csvFilePath) === false) {
            throw new InvalidArgumentException(sprintf('%s is not file.', $csvFilePath), self::ERROR_IS_NOT_FILE);
        }

        if (is_readable($csvFilePath) === false) {
            throw new InvalidArgumentException(sprintf('%s is not readable.', $csvFilePath), self::ERROR_IS_NOT_READABLE);
        }

        $this->fileResource = fopen($csvFilePath, 'r');
        if ($this->fileResource === false) {
            throw new InvalidArgumentException(sprintf('Cannot open file %s to read', $csvFilePath), self::ERROR_CANNOT_OPEN_FILE);
        }
        $this->delimiter = $delimiter;
        $this->rowLength = $rowLength;
    }

    public function current(): array
    {
        return $this->csvRowData;
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        $this->csvRowData = fgetcsv($this->fileResource, $this->rowLength, $this->delimiter);
        return is_array($this->csvRowData);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}
