<?php declare(strict_types=1);

namespace ulekare\repository;

use Nette\Database\Connection;
use ulekare\entity\Vote;

class VoteRepository
{

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param Vote[] $votes
     */
    public function addMultiple(array $votes): void
    {
        $questions = [];
        $responses = [];

        foreach ($votes as $vote) {
            if (in_array($vote->getQuestion(), $questions, true) === false) {
                $questions[] = ['question' => $vote->getQuestion()];
            }

            if (in_array($vote->getResponse(), $responses, true) === false) {
                $responses[] = ['response' => $vote->getResponse()];
            }
            $poll[] = $vote->toArray();
        }

        $this->addQuestions($questions);
        $this->addResponses($responses);

        $questions = $this->connection->query('SELECT * FROM question WHERE question IN (?)', $questions)->fetchPairs('id', 'question');
        $responses = $this->connection->query('SELECT * FROM response WHERE response IN (?)', $responses)->fetchPairs('id', 'response');

        $poll = array_map(function ($item) use ($responses, $questions) {
            $item['response_id'] = array_search($item['response'], $responses);
            $item['question_id'] = array_search($item['question'], $questions);
            unset($item['question'], $item['response']);
            return $item;
        }, $poll);

        $this->connection->query('INSERT INTO poll_log ?', $poll);
    }

    private function addQuestions(array $questions): void
    {
        $this->connection->query('INSERT IGNORE INTO question ?', $questions);
    }

    private function addResponses(array $responses): void
    {
        $this->connection->query('INSERT IGNORE INTO response ?', $responses);
    }
}
