<?php declare(strict_types=1);

namespace ulekare\helper;

use DateTime;

class Time
{
    public static function elapsed(DateTime $date)
    {
        $secs = (time() - $date->getTimestamp());
        $bit = array(
            'y' => $secs / 31556926 % 12,
            'w' => $secs / 604800 % 52,
            'd' => $secs / 86400 % 7,
            'h' => $secs / 3600 % 24,
            'm' => $secs / 60 % 60,
            's' => $secs % 60
        );

        foreach ($bit as $k => $v)
            if ($v > 0) $ret[] = $v . $k;

        return join(' ', $ret);
    }
}
